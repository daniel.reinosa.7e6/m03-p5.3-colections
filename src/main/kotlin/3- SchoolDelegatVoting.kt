fun main() {
    val delegats= mutableSetOf<String>()
    val votacions= mutableMapOf<String,Int>()
    var vot=""
    while (vot.uppercase()!="END"){
        vot=sc.next()
        if(vot.uppercase()=="END")break
        if(delegats.add(vot)) {
            delegats.add(vot)
            votacions.put(vot, 1)
        }
        else votacions[vot] = votacions[vot]!! + 1
    }
    println(votacions)
}
